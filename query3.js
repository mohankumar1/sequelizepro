const Sequelize = require('sequelize');
const connections = require('./server.js').connection;
const matchesTable = require('./matchesModule.js').matchesTable
const Match = matchesTable(connections());
const deliveriesTable = require('./deliveriesModule.js').deliveriesTable
const deliveries = deliveriesTable(connections());

deliveries.belongsTo(Match, {
    foreignKey: 'match_id'
});
deliveries.findAll({
    include: [{

        model: Match,
        where: {
            season: '2016',
        },
        require: true,
        attributes: []

    }],
    raw: true,
    attributes: ['bowling_team',
        [Sequelize.fn('sum',
                deliveries.sequelize.col('extra_runs')),
            'extraRuns'
        ]
    ],
    group: ["bowling_team"]
}).then((result) => {
    console.log(result);
})