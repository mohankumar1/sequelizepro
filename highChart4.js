let season =['RN ten Doeschate','J Yadav','V Kohli','R Ashwin','S Nadeem','MC Henriques','Z Khan','Parvez Rasool','MA Starc', 'Sandeep Sharma',];
let matches =[ 3.4285714285714284 , 4.142857142857143 , 5.454545454545454 , 5.7 , 5.863636363636363 , 6.038216560509, 6.038709677419355, 6.2, 6.41911764705882, 6.752411575562701 ]
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'top 10 economy bowlers'
    },
    xAxis: {
        categories: season,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'matches played',
        data: matches
    }]

});