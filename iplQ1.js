const Sequelize = require('sequelize');
const sequelize = new Sequelize('iplDataBase', 'root', 'root', {
    host: 'localhost',
    dialect: 'mysql',
    define: {
        timestamps: false
    }
});
sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });


const Match = sequelize.define('iplmatch', {

    id: {
        type: Sequelize.INTEGER,
        primaryKey: true
    },
    season: Sequelize.INTEGER,
    city: Sequelize.STRING,
    date: Sequelize.INTEGER,
    team1: Sequelize.STRING,
    team2: Sequelize.STRING,
    toss_winner: Sequelize.STRING,
    toss_decision: Sequelize.STRING,
    result: Sequelize.STRING,
    dl_applied: Sequelize.INTEGER,
    winner: Sequelize.STRING,
    win_by_runs: Sequelize.INTEGER,
    win_by_wickets: Sequelize.INTEGER,
    player_of_match: Sequelize.STRING,
    venue: Sequelize.STRING,
    umpire1: Sequelize.STRING,
    umpire2: Sequelize.STRING,
    umpire3: Sequelize.STRING,
}, {});
const deliveries = sequelize.define('iplDeliverie', {

    match_id: {
        type: Sequelize.INTEGER,
        references: {
            model: Match,
            key: 'id'
        }
    },
    inning: Sequelize.INTEGER,
    batting_team: Sequelize.STRING,
    bowling_team: Sequelize.STRING,
    over: Sequelize.INTEGER,
    ball: Sequelize.INTEGER,
    batsman: Sequelize.STRING,
    non_striker: Sequelize.STRING,
    bowler: Sequelize.STRING,
    is_super_over: Sequelize.INTEGER,
    wide_runs: Sequelize.INTEGER,
    bye_runs: Sequelize.INTEGER,
    legbye_runs: Sequelize.INTEGER,
    noball_runs: Sequelize.INTEGER,
    penalty_runs: Sequelize.INTEGER,
    batsman_runs: Sequelize.INTEGER,
    extra_runs: Sequelize.INTEGER,
    total_runs: Sequelize.INTEGER,
    player_dismissed: Sequelize.INTEGER,
    dismissal_kind: Sequelize.STRING,
    fielder: Sequelize.STRING,

}, {});
deliveries.belongsTo(Match, {
    foreignKey: 'match_id'
});
deliveries.findAll({
    include: [{

        model: Match,
        where: {
            season: '2016',
        },
        require: true,
        attributes: []

    }],
    raw: true,
    attributes: ['bowling_team',
        [sequelize.fn('sum',
                deliveries.sequelize.col('extra_runs')),
            'extraRuns'
        ]
    ],
    group: ["bowling_team"]
}).then((result) => {
    console.log(result);
})
deliveries.findAll({
    include: [{

        model: Match,
        where: {
            season: '2015',
        },
        require: true,
        attributes: []
    }],
    raw: true,
    attributes: ['bowler',
        [sequelize.fn('sum',
            deliveries.sequelize.col('total_runs')), "totalRuns"],
        [sequelize.fn('count',
            deliveries.sequelize.col('total_runs')), "balls"],
        [sequelize.fn('sum',
            deliveries.sequelize.col('bye_runs')), "byeRuns"],
        [sequelize.fn('sum',
            deliveries.sequelize.col('legbye_runs')), "legByeRuns"],
    ],
    group: ["bowler"]
}).then((result) => {
    let output = [];
    let economyRate = [];
    let bowler = [];
    let runsConsidered = []
    for (var i in result) {
        let bowlerDetails=result[i];
        bowler[i] = bowlerDetails["bowler"];
        runsConsidered[i] = Number(bowlerDetails["totalRuns"]) -
            Number(bowlerDetails["byeRuns"]) - Number(bowlerDetails["legByeRuns"])
        economyRate[i] = runsConsidered[i] * 6 / Number(bowlerDetails["balls"]);
        output.push([bowler[i], economyRate[i]])
    }
    output.sort((a, b) => {

        return a[1] - b[1]

    })

    console.log(output.slice(0, 10));
})
Match.findAll({

    raw: true,
    attributes: ['season',
        [sequelize.fn('count', sequelize.col('Id')), 'count']
    ],
    group: ["season"]

}).then((result) => {

    console.log(result);
})
Match.findAll({

    raw: true,
    attributes: ['season', 'winner',
        [sequelize.fn('count', sequelize.col('Id')), 'count']
    ],
    group: ["season", 'winner']

}).then((result) => {

    console.log(result);
})