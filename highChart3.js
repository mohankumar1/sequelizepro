let team =['Delhi Daredevils','Gujarat Lions','Kings XI Punjab','Kolkata Knight Riders','Mumbai Indians','Rising Pune Supergiants','Royal Challengers Bangalore','Sunrisers Hyderabad', ];
let extarRuns=[106 ,98 ,100 ,122 ,102 ,108 ,156 ,107]
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'extra runs given by each team'
    },
    xAxis: {
        categories: team,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'extarRuns',
        data: extarRuns
    }]

});