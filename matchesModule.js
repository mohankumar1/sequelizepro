const Sequelize = require('sequelize');
// const connections = require('./server.js').connection;


function matchesTable(sequelize) {
    let Match = sequelize.define('iplmatch', {

        id: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        season: Sequelize.INTEGER,
        city: Sequelize.STRING,
        date: Sequelize.INTEGER,
        team1: Sequelize.STRING,
        team2: Sequelize.STRING,
        toss_winner: Sequelize.STRING,
        toss_decision: Sequelize.STRING,
        result: Sequelize.STRING,
        dl_applied: Sequelize.INTEGER,
        winner: Sequelize.STRING,
        win_by_runs: Sequelize.INTEGER,
        win_by_wickets: Sequelize.INTEGER,
        player_of_match: Sequelize.STRING,
        venue: Sequelize.STRING,
        umpire1: Sequelize.STRING,
        umpire2: Sequelize.STRING,
        umpire3: Sequelize.STRING,
    }, {});
    return Match;
}
// matchesTable(connections)
module.exports = {
    matchesTable: matchesTable
}