let season =[2008,2009,2010, 2011, 2012,2013,2014,2015,2016, 2017];
let matches =[58 ,57 ,60 ,73 ,74 ,76 ,60 ,59 ,60 ,59 ]
Highcharts.chart('container', {
    chart: {
        type: 'bar'
    },
    title: {
        text: 'matches played per year'
    },
    xAxis: {
        categories: season,
        title: {
            text: null
        }
    },
    yAxis: {
        min: 0,
        title: {
            text: 'Population (millions)',
            align: 'high'
        },
        labels: {
            overflow: 'justify'
        }
    },
    tooltip: {
        valueSuffix: ' millions'
    },
    plotOptions: {
        bar: {
            dataLabels: {
                enabled: true
            }
        }
    },
    legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        x: -40,
        y: 80,
        floating: true,
        borderWidth: 1,
        backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || '#FFFFFF',
        shadow: true
    },
    credits: {
        enabled: false
    },
    series: [{
        name: 'matches played',
        data: matches
    }]

});