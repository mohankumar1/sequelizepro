const Sequelize = require('sequelize');

function connection() {
    let sequelize = new Sequelize('iplDataBase', 'root', 'root', {
        host: 'localhost',
        dialect: 'mysql',
        define: {
            timestamps: false
        }
    });
    sequelize
        .authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
        });
    // console.log(sequelize)

    return sequelize;
}
// connection()
module.exports = {
    connection: connection
}