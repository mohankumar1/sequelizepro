const Sequelize = require('sequelize');
const matchesTable = require('./matchesModule.js').matchesTable;
const connections = require('./server.js').connection;
const Match = matchesTable(connections());

function deliveriesTable(sequelize) {
    const deliveries = sequelize.define('iplDeliverie', {

        match_id: {
            type: Sequelize.INTEGER,
            references: {
                model: Match,
                key: 'id'
            }
        },
        inning: Sequelize.INTEGER,
        batting_team: Sequelize.STRING,
        bowling_team: Sequelize.STRING,
        over: Sequelize.INTEGER,
        ball: Sequelize.INTEGER,
        batsman: Sequelize.STRING,
        non_striker: Sequelize.STRING,
        bowler: Sequelize.STRING,
        is_super_over: Sequelize.INTEGER,
        wide_runs: Sequelize.INTEGER,
        bye_runs: Sequelize.INTEGER,
        legbye_runs: Sequelize.INTEGER,
        noball_runs: Sequelize.INTEGER,
        penalty_runs: Sequelize.INTEGER,
        batsman_runs: Sequelize.INTEGER,
        extra_runs: Sequelize.INTEGER,
        total_runs: Sequelize.INTEGER,
        player_dismissed: Sequelize.INTEGER,
        dismissal_kind: Sequelize.STRING,
        fielder: Sequelize.STRING,

    }, {});

    return deliveries;
}
module.exports = {
    deliveriesTable: deliveriesTable
}