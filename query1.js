const Sequelize = require('sequelize');
const connections = require('./server.js').connection;
const matchesTable = require('./matchesModule.js').matchesTable
const Match = matchesTable(connections());

Match.findAll({

    raw: true,
    attributes: ['season',
        [Sequelize.fn('count', Sequelize.col('Id')), 'count']
    ],
    group: ["season"]

}).then((result) => {

    console.log(result);
   
})


