const Sequelize = require('sequelize');
const connections = require('./server.js').connection;
const matchesTable = require('./matchesModule.js').matchesTable
const Match = matchesTable(connections());
const deliveriesTable = require('./deliveriesModule.js').deliveriesTable
const deliveries = deliveriesTable(connections());

deliveries.belongsTo(Match, {
    foreignKey: 'match_id'
});

deliveries.findAll({
    include: [{

        model: Match,
        where: {
            season: '2015',
        },
        require: true,
        attributes: []
    }],
    raw: true,
    attributes: ['bowler',
        [Sequelize.fn('sum',
            deliveries.sequelize.col('total_runs')), "totalRuns"],
        [Sequelize.fn('count',
            deliveries.sequelize.col('total_runs')), "balls"],
        [Sequelize.fn('sum',
            deliveries.sequelize.col('bye_runs')), "byeRuns"],
        [Sequelize.fn('sum',
            deliveries.sequelize.col('legbye_runs')), "legByeRuns"],
    ],
    group: ["bowler"]
}).then((result) => {
    let output = [];
    let economyRate = [];
    let bowler = [];
    let runsConsidered = []
    for (var i in result) {
        let bowlerDetails = result[i];
        bowler[i] = bowlerDetails["bowler"];
        runsConsidered[i] = Number(bowlerDetails["totalRuns"]) -
            Number(bowlerDetails["byeRuns"]) - Number(bowlerDetails["legByeRuns"])
        economyRate[i] = runsConsidered[i] * 6 / Number(bowlerDetails["balls"]);
        output.push([bowler[i], economyRate[i]])
    }
    output.sort((a, b) => {

        return a[1] - b[1]

    })

    console.log(output.slice(0, 10));
})